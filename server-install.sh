#!/bin/bash 

patch=`echo $0 | rev | sed -r 's/^[^/]+//' | cut -c 2- | rev`

echo "Это мастер настройки нового сервера. Хотите ли подготовить свой сервер в автоматическом режиме, следуя инструкциям?"
echo "Eto master nastrojki novogo servera. Xotite li podgotovit svoj server v avtomaticheskom rezhime, sleduya instrukciyam?"
echo "y/n"
read who
if [ "$who" = "n" ]; then exit 0; elif [ "$who" = "y" ]; then echo "" > /dev/null; else echo "Abort" && exit 0; fi

echo "Произвести настройку русской локали?"
echo "Proizvesti nastrojku russkoj lokali?"
echo "y/n"
read who
if ! [ "$who" = "y" ] || [ "$who" = "n" ]; then echo "Abort" && exit 0; fi
if [ "$who" = "y" ]; then
echo "Выбирайте ru_RU.UTF-8"
echo "Vybirajte ru_RU.UTF-8"
sleep 3
dpkg-reconfigure locales
apt-get install console-cyrillic
dpkg-reconfigure console-cyrillic; fi

echo "Установить Java 8 из ppa webupd8team?"
echo "Ustanovit Java 8 iz ppa webupd8team?"
echo "y/n"
read who
if ! [ "$who" = "y" ] || [ "$who" = "n" ]; then echo "Abort" && exit 0; fi
if [ "$who" = "y" ]; then
echo "deb http://ppa.launchpad.net/webupd8team/java/ubuntu precise main" >> /etc/apt/sources.list
apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 7B2C3B0889BF5709A105D03AC2518248EEA14886
apt-get update
apt-get install oracle-java8-installer
update-java-alternatives -s java-8-oracle; fi

if [ "$who" = "n" ]; then
echo "Установить Java 7?"
echo "Ustanovit Java 7?"
echo "y/n"
read who
if ! [ "$who" = "y" ] || [ "$who" = "n" ]; then echo "Abort" && exit 0; fi
if [ "$who" = "y" ]; then apt-get install openjdk-7-jre; fi; fi

echo "Создать SWAP файл?"
echo "Sozdat SWAP fajl?"
echo "y/n"
read who
if ! [ "$who" = "y" ] || [ "$who" = "n" ]; then echo "Abort" && exit 0; fi
if [ "$who" = "y" ]; then
echo "Введите размер в гигабайтах"
echo "Vvedite razmer v gigabajtax"
read size
size=$(echo $size | sed s/[^0-9]//g)
echo $size"G"
dd if=/dev/zero of=/.swapfile.tmp bs=1G count=$size
mkswap /.swapfile.tmp
swapon /.swapfile.tmp
echo "/.swapfile.tmp none swap sw 0 0" >> /etc/fstab; fi

echo "Повысить безопасность?"
echo "Povysit bezopasnost?"
echo "y/n"
read who
if ! [ "$who" = "y" ] || [ "$who" = "n" ]; then echo "Abort" && exit 0; fi
if [ "$who" = "y" ]; then
echo "Введите желаемый порт ssh"
echo "Vvedite zhelaemyj port ssh"
read sshport
if [ -f /etc/ssh/sshd_config ]; then
sed "s/Port 22/Port $sshport/g" /etc/ssh/sshd_config >> /etc/ssh/sshd_config.new
mv /etc/ssh/sshd_config /etc/ssh/sshd_config.old
mv /etc/ssh/sshd_config.new /etc/ssh/sshd_config; fi
service ssh restart
echo "Введите желаемый порт ftp"
echo "Vvedite zhelaemyj port ftp"
read ftpport
if [ -f /etc/vsftpd.conf ]; then echo "listen_port="$ftpport >> /etc/vsftpd.conf; fi
service vsftpd restart
apt-get install fail2ban
if [ -f /etc/fail2ban/jail.conf ]; then sed "s/port     = ssh/port     = $sshport/g" /etc/fail2ban/jail.conf > /dev/null; fi
service fail2ban restart
echo "Прочтите эту статью:"
echo "Prochtite etu statyu:"
echo "http://putty.org.ru/articles/fail2ban-ssh.html"; fi

echo "Проверить наличие зависимостей, необходимых Vominator?"
echo "Proverit nalichie vsex zavisimostej, neobxodimyx Vominator?"
echo "y/n"
read who
if ! [ "$who" = "y" ] || [ "$who" = "n" ]; then echo "Abort" && exit 0; fi
if [ "$who" = "y" ]; then
pack=zip
check=$(dpkg -l | grep $pack | wc -m | sed s/[^0-9]//g)
if [ "$check" -lt 2 ]; then apt-get install $pack; fi
# pack=awk
# check=$(dpkg -l | grep $pack | wc -m | sed s/[^0-9]//g)
# if [ "$check" -lt 2 ]; then apt-get install $pack; fi
# pack=tail
# check=$(dpkg -l | grep $pack | wc -m | sed s/[^0-9]//g)
# if [ "$check" -lt 2 ]; then apt-get install $pack; fi
pack=sed
check=$(dpkg -l | grep $pack | wc -m | sed s/[^0-9]//g)
if [ "$check" -lt 2 ]; then apt-get install $pack; fi
# pack=head
# check=$(dpkg -l | grep $pack | wc -m | sed s/[^0-9]//g)
# if [ "$check" -lt 2 ]; then apt-get install $pack; fi
# pack=bash
# check=$(dpkg -l | grep $pack | wc -m | sed s/[^0-9]//g)
# if [ "$check" -lt 2 ]; then apt-get install $pack; fi
# pack=dash
# check=$(dpkg -l | grep $pack | wc -m | sed s/[^0-9]//g)
# if [ "$check" -lt 2 ]; then apt-get install $pack; fi
# pack=find
# check=$(dpkg -l | grep $pack | wc -m | sed s/[^0-9]//g)
# if [ "$check" -lt 2 ]; then apt-get install $pack; fi
# pack=tee
# check=$(dpkg -l | grep $pack | wc -m | sed s/[^0-9]//g)
# if [ "$check" -lt 2 ]; then apt-get install $pack; fi
pack=screen
check=$(dpkg -l | grep $pack | wc -m | sed s/[^0-9]//g)
if [ "$check" -lt 2 ]; then apt-get install $pack; fi
pack=wget
check=$(dpkg -l | grep $pack | wc -m | sed s/[^0-9]//g)
if [ "$check" -lt 2 ]; then apt-get install $pack; fi
# pack=grep
# check=$(dpkg -l | grep $pack | wc -m | sed s/[^0-9]//g)
# if [ "$check" -lt 2 ]; then apt-get install $pack; fi
# pack=stat
# check=$(dpkg -l | grep $pack | wc -m | sed s/[^0-9]//g)
# if [ "$check" -lt 2 ]; then apt-get install $pack; fi
pack=cadaver
check=$(dpkg -l | grep $pack | wc -m | sed s/[^0-9]//g)
if [ "$check" -lt 2 ]; then apt-get install $pack; fi
pack=davfs2
check=$(dpkg -l | grep $pack | wc -m | sed s/[^0-9]//g)
if [ "$check" -lt 2 ]; then apt-get install $pack; fi
pack=netcat
check=$(dpkg -l | grep $pack | wc -m | sed s/[^0-9]//g)
if [ "$check" -lt 2 ]; then apt-get install $pack; fi; fi

echo "Проверка global.cfg"
echo "Proverka global.cfg"
errorch=$(grep errorch= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
if [ $errorch = "1" ]; then $patch/sh/check.sh $$; elif [ $errorch = "0" ]; then $patch/sh/check.sh $$; else
echo "Не верное значение переменной errorch в конфиге"
echo "Ne vernoe znachenie peremennoj errorch v konfige"
exit 0; fi

echo "Настройка завершена!"
echo "Nastrojka zavershena!"