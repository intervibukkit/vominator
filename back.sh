#!/bin/bash
patch=`echo $0 | rev | sed -r 's/^[^/]+//' | cut -c 2- | rev`
date=`/bin/date '+%H:%M:%S-%d.%m.%Y'`

errorch=$(grep errorch= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
if [ $errorch = "1" ]; then $patch/sh/check.sh $$; elif [ $errorch = "0" ]; then echo "Проверка конфига не включена"; else
echo "Не верное значение переменной errorch в конфиге"
exit 0; fi

start=$(grep start= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
gufil=$(grep gufil= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
backups=$(grep backups= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
plugzip=$(grep plugzip= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
bfile=$(grep bfile= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
bdirs=$(grep bdirs= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
yadisk=$(grep yadisk= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
compress=$(grep compress= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
yascres=$(grep yascres= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
bscr=$(grep bscr= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
tbtoosn=$(grep tbtoosn= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
bmode=$(grep bmode= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)

echo '=====================> начало бэкапирования: '$date >> $patch/logs/backup.log

if ! [ -d $patch/backups/ ]; then mkdir $patch/backups/ >> $patch/logs/backup.log; fi

cd $patch/backups/
num=$(ls -l | grep ^d |wc -l)
old=$(ls -t $patch/backups/ | tail -n1)
new=$(ls -t -1rF | grep / | tail -1 | tr -d [=/=])
newch=$(echo $new | wc -m | sed s/[^0-9]//g)
if [ "$newch" -lt 3 ]; then new=none; fi

echo 'бэкапов в папке: '$num

while [ "$((num+1))" -gt $backups ]; do
echo "удаление старого бэкапа "$old
echo "удаление старого бэкапа "$old >> $patch/logs/backup.log
rm -rf $patch/backups/$old/ >> $patch/logs/backup.log
num=$(ls -l | grep ^d |wc -l)
old=$(ls -t $patch/backups/ | tail -n1); done

echo 'создание папки '$date
echo 'создание папки '$date >> $patch/logs/backup.log
mkdir $patch/backups/$date >> $patch/logs/backup.log

line=$(wc -l $patch/cfg/worlds.cfg | sed s/[^0-9]//g)
step=1

echo 'строк в worlds.cfg: '$line >> $patch/logs/backup.log

while [ $((line+2)) != $step ]; do
mir=$(head -n $step $patch/cfg/worlds.cfg | tail -n 1)

if [ "$1" = "z" ]; then
echo 'архивирование карты' $mir
echo 'архивирование карты' $mir >> $patch/logs/backup.log
zip -r -$compress $patch/backups/$date/$mir.zip $patch/$mir/ >> $patch/logs/backup.log; else
echo 'копирование карты' $mir
echo 'копирование карты' $mir >> $patch/logs/backup.log
cp -r $patch/$mir/ $patch/backups/$date/$mir/ >> $patch/logs/backup.log; fi
step=$((step+1))
# экстренный прерыватель
if [ $step -gt $((line+3)) ]; then step=$(echo $((line+2))) && break; fi; done


if [ "$plugzip" = "1" ]; then
echo 'архивирование плагинов'
echo 'архивирование плагинов' >> $patch/logs/backup.log
zip -r -$compress $patch/backups/$date/plugins.zip $patch/plugins/ >> $patch/logs/backup.log; else
echo 'копирование плагинов'
echo 'копирование плагинов' >> $patch/logs/backup.log
cp -r $patch/plugins/ $patch/backups/$date/plugins/ >> $patch/logs/backup.log; fi

if [ $gufil = "1" ]; then
if [ -d $patch/temp/server-files/ ]; then
rm -rf $patch/temp/server-files/
mkdir $patch/temp/server-files/; else mkdir $patch/temp/server-files/; fi
if [ -f $patch/bukkit.yml ]; then cp $patch/bukkit.yml $patch/temp/server-files/bukkit.yml; fi
if [ -f $patch/commands.yml ]; then cp $patch/commands.yml $patch/temp/server-files/commands.yml; fi
if [ -f $patch/eula.txt ]; then cp $patch/eula.txt $patch/temp/server-files/eula.txt; fi
if [ -f $patch/help.yml ]; then cp $patch/help.yml $patch/temp/server-files/help.yml; fi
if [ -f $patch/ops.json ]; then cp $patch/ops.json $patch/temp/server-files/ops.json; fi
if [ -f $patch/permissioms.yml ]; then cp $patch/permissioms.yml $patch/temp/server-files/permissions.yml; fi
if [ -f $patch/server.properties ]; then cp $patch/server.properties $patch/temp/server-files/server.properties; fi
if [ -f $patch/spigot.yml ]; then cp $patch/spigot.yml $patch/temp/server-files/spigot.yml; fi
if [ -f $patch/usercache.json ]; then cp $patch/usercache.json $patch/temp/server-files/usercache.json; fi
if [ -f $patch/wepif.yml ]; then cp $patch/wepif.yml $patch/temp/server-files/wepif.yml; fi
if [ -f $patch/whitelist.json ]; then cp $patch/whitelist.json $patch/temp/server-files/whitelist.json; fi
if [ -f $patch/ops.txt ]; then cp $patch/ops.txt $patch/temp/server-files/ops.txt; fi
if [ -f $patch/whitelist.txt ]; then cp $patch/whitelist.txt $patch/temp/server-files/whitelist.txt; fi
if [ -f $patch/banned-ips.json ]; then cp $patch/banned-ips.json $patch/temp/server-files/banned-ips.json; fi
if [ -f $patch/banned-players.json ]; then cp $patch/banned-players.json $patch/temp/server-files/banned-players.json; fi
if [ -f $patch/banlist.txt ]; then cp $patch/banlist.txt $patch/temp/server-files/banlist.txt; fi
if [ -f $patch/banip.txt ]; then cp $patch/banip.txt $patch/temp/server-files/banip.txt; fi
if [ -f $patch/$start ]; then cp $patch/$start $patch/temp/server-files/$start; fi
if [ -d $patch/cfg/ ]; then cp -r $patch/cfg/ $patch/temp/server-files/cfg/; fi
if [ -d $patch/logs/ ]; then cp -r $patch/logs/ $patch/temp/server-files/logs/; fi
if [ -d $patch/crash-reports/ ]; then cp -r $patch/crash-reports/ $patch/temp/server-files/crash-reports/; fi
if [ -d $patch/timings/ ]; then cp -r $patch/timings/ $patch/temp/server-files/timings/; fi
if [ -d $patch/RainbowData/ ]; then cp -r $patch/RainbowData/ $patch/temp/server-files/RainbowData/; fi
if [ -f $patch/Rainbow.properties ]; then cp $patch/Rainbow.properties $patch/temp/server-files/Rainbow.properties; fi
if [ -d $patch/AsyncLog/ ]; then cp -r $patch/AsyncLog/ $patch/temp/server-files/AsyncLog/; fi
if [ -d $patch/Backpacks/ ]; then cp -r $patch/Backpacks/ $patch/server-files/Backpacks/; fi
if [ -f $patch/sh/update-ch.cfg ]; then cp $patch/sh/update-ch.cfg $patch/temp/server-files/update-ch.cfg; fi
if [ "$bmode" = "1" ] || [ "$1" = "z" ]; then
echo "архивирование важных файлов"
echo "архивирование важных файлов" >> $patch/logs/backup.log
zip -r -$compress $patch/backups/$date/server-files.zip $patch/temp/server-files/ >> $patch/logs/backup.log && rm -rf $patch/temp/server-files/; else
echo "копирование важных файлов"
echo "копирование важных файлов" >> $patch/logs/backup.log
mv $patch/temp/server-files/ $patch/backups/$date/server-files/ >> $patch/logs/backup.log; fi; fi

if [ "$bfile" = "1" ]; then
line=$(wc -l $patch/cfg/back-file.cfg | sed s/[^0-9]//g)
step=1
if [ "$1" = "z" ]; then
while [ $((line+2)) != $step ]; do
file=`head -n $step $patch/cfg/back-file.cfg | tail -n 1`
head -n $step $patch/cfg/back-file.cfg | tail -n 1 >> $patch/logs/backup.log
if [ -f $file ]; then
echo 'архивирование файла:' $file
name=${file##*/}
zip -r -$compress $patch/backups/$date/$name.zip $file >> $patch/logs/backup.log; else echo "Нет такого файла:" $file; fi
step=$((step+1))
# экстренный прерыватель
if [ $step -gt $((line+3)) ]; then step=$(echo $((line+2))) && break; fi; done; else
while [ $((line+2)) != $step ]; do
file=`head -n $step $patch/cfg/back-file.cfg | tail -n 1`
head -n $step $patch/cfg/back-file.cfg | tail -n 1 >> $patch/logs/backup.log
if [ -f $file ]; then
echo 'копирование файла:' $file
name=${file##*/}
cp -r $file $patch/backups/$date/$name >> $patch/logs/backup.log; else echo "Нет такого файла:" $file; fi
step=$((step+1))
# экстренный прерыватель
if [ $step -gt $((line+2)) ]; then step=$(echo $((line+2))) && break; fi; done; fi; fi

if [ "$bdirs" = "1" ]; then
line=$(wc -l $patch/cfg/back-dir.cfg | sed s/[^0-9]//g)
step=1
if [ "$1" = "z" ]; then
while [ $((line+2)) != $step ]; do
file=`head -n $step $patch/cfg/back-dir.cfg | tail -n 1`
head -n $step $patch/cfg/back-file.cfg | tail -n 1 >> $patch/logs/backup.log
if [ -d $file ]; then
echo 'архивирование папки:' $file
name=`echo $file | rev | cut -f1 -d/ | rev`
zip -r -$compress $patch/backups/$date/$name.zip $file >> $patch/logs/backup.log; else echo "Нет такой папки:" $file; fi
step=$((step+1))
# экстренный прерыватель
if [ $step -gt $((line+3)) ]; then step=$(echo $((line+2))) && break; fi; done; else
while [ $((line+2)) != $step ]; do
file=`head -n $step $patch/cfg/back-dir.cfg | tail -n 1`
head -n $step $patch/cfg/back-dir.cfg | tail -n 1 >> $patch/logs/backup.log
if [ -d $file ]; then
echo 'копирование папки:' $file
name=`echo $file | rev | cut -f1 -d/ | rev`
cp -r $file $patch/backups/$date/$name >> $patch/logs/backup.log; else echo "Нет такой папки:" $file; fi
step=$((step+1))
# экстренный прерыватель
if [ $step -gt $((line+2)) ]; then step=$(echo $((line+2))) && break; fi; done; fi; fi

if [ $tbtoosn = "1" ] && [ $yadisk = "1" ]; then
echo "архивирование временных бэкапов"
echo "архивирование временных бэкапов" >> $patch/logs/backup.log
zip -r -$compress $patch/backups/$date/temp-backups.zip $patch/backups-temp/ >> $patch/logs/backup.log; fi

if [ $yadisk = "1" ] && [ $yascres = "0" ] && [ $bscr = "0" ]; then
echo 'синхронизация с yandex disk'
echo 'синхронизация с yandex disk' >> $patch/logs/backup.log
if [ $new = "none" ]; then $patch/sh/yasync.sh $patch/backups/$date; else $patch/sh/yasync.sh $patch/backups/$date $patch/backups/$new; fi; fi
if [ $yadisk = "1" ] && [ $yascres = "0" ] && [ $bscr = "1" ]; then
echo 'синхронизация с yandex disk'
echo 'синхронизация с yandex disk' >> $patch/logs/backup.log
if [ $new = "none" ]; then $patch/sh/yasync.sh $patch/backups/$date; else $patch/sh/yasync.sh $patch/backups/$date $patch/backups/$new; fi; fi
if [ $yadisk = "1" ] && [ $yascres = "1" ] && [ $bscr = "1" ]; then
echo 'синхронизация с yandex disk'
echo 'синхронизация с yandex disk' >> $patch/logs/backup.log
if [ $new = "none" ]; then $patch/sh/yasync.sh $patch/backups/$date; else $patch/sh/yasync.sh $patch/backups/$date $patch/backups/$new; fi; fi
if [ $yadisk = "1" ] && [ $yascres = "1" ] && [ $bscr = "0" ]; then
if ! [ -d $patch/temp/back/ ]; then mkdir $patch/temp/back/; fi
echo "$patch/backups/$date" > $patch/temp/back/back.cfg
echo "$patch/backups/$new" >> $patch/temp/back/back.cfg; fi

echo 'бэкапирование завершено'
date=`/bin/date '+%H:%M:%S-%d.%m.%Y'`
echo "==================== бэкапирование завершено:" $date "====================" >> $patch/logs/backup.log