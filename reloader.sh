#!/bin/bash 

patch=`echo $0 | rev | sed -r 's/^[^/]+//' | cut -c 2- | rev`
date=`/bin/date '+%H:%M:%S-%d.%m.%Y'`

errorch=$(grep errorch= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
if [ $errorch = "1" ]; then $patch/sh/check.sh $$; elif [ $errorch = "0" ]; then echo "Проверка конфига не включена"; else
echo "Не верное значение переменной errorch в конфиге"
exit 0; fi

aumes=$(grep aumes= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
autsav=$(grep autsav= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
ticms=$(grep ticms= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
tsto=$(grep tsto= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
ansto=$(grep ansto= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
inscr=$(grep inscr= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
antichat=$(grep antichat= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
anthascr=$(grep anthascr= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
tbascnz=$(grep tbascnz= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
smtscr=$(grep smtscr= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)

echo "какой из модулей вы хотите перезагрузить?"
echo "1 - авто-сообщения"
echo "2 - авто-сохранение"
echo "3 - выполнение команд по времени"
echo "4 - остановка сервера в заданное время"
echo "5 - антистоп (защита от зависания/краша)"
echo "6 - выполнение команд по интервалу"
echo "7 - защита чата"
echo "8 - защита от брута паролей игроков"
echo "9 - временные бэкапы"
echo "10 - умная защита"
read reb

if [ $reb = "1" ]; then
echo "перезапуск automessage.sh"
screen -S $aumes -X stuff "^C"
sleep 3
screen -dmUS $aumes bash -c "sleep 120 && $patch/sh/automessage.sh"; elif [ $reb = "2" ]; then
echo "перезапуск autosave.sh"
screen -S $autsav -X stuff "^C"
sleep 3
screen -dmUS $autsav bash -c "sleep 120 && $patch/sh/autosave.sh"; elif [ $reb = "3" ]; then
echo "перезапуск timecommand.sh"
screen -S $ticms -X stuff "^C"
sleep 3
screen -dmUS $ticms bash -c "sleep 120 && $patch/sh/timecommand.sh"; elif [ $reb = "4" ]; then
echo "перезапуск timestop.sh"
screen -S $tsto -X stuff "^C"
sleep 3
screen -dmUS $tsto bash -c "sleep 120 && $patch/sh/timestop.sh"; elif [ $reb = "5" ]; then
echo "перезапуск antistop.sh & antistoper.sh"
screen -S $ansto -X stuff "^C"
sleep 3
if [ -f $patch/temp/antistop.pid ]; then
aspid=$(head -n 1 $patch/temp/antistop.pid| tail -n 1)
kill -s KILL $aspid >> $patch/logs/antistop.log
rm $patch/temp/antistop.pid >> $patch/logs/antistop.log; fi
sh $patch/sh/antistop.sh n &
disown
screen -dmUS $ansto bash -c "$patch/sh/antistoper.sh"; elif [ $reb = "6" ]; then
echo "перезапуск intcom.sh"
screen -S $inscr -X stuff "^C"
sleep 3
screen -dmUS $inscr bash -c "sleep 120 && $patch/sh/intcom.sh"; elif [ $reb = "7" ]; then
echo "перезапуск chat.sh"
screen -S $antichat -X stuff "^C"
sleep 3
screen -dmUS $antichat bash -c "sleep 120 && $patch/sh/chat.sh"; elif [ $reb = "8" ]; then
echo "перезапуск antihack.sh"
screen -S $anthascr -X stuff "^C"
sleep 3
screen -dmUS $anthascr bash -c "sleep 120 && $patch/sh/antihack.sh"; elif [ $reb = "9" ]; then
echo "перезапуск tempback.sh"
screen -S $tbascnz -X stuff "^C"
sleep 3
screen -dmUS $tbascnz bash -c "$patch/sh/tempback.sh"; elif [ $reb = "10" ]; then
echo "перезапуск smart.sh"
screen -S $smtscr -X stuff "^C"
sleep 3
screen -dmUS $smtscr bash -c "$patch/sh/smart.sh"; else echo "не верный ввод"; fi