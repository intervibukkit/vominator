#!/bin/bash
patch=`echo $0 | rev | sed -r 's/^[^/]+//' | cut -c 2- | rev`

# нужно для разового запуска, в цикле переменная проверяется из конфига
# поэтому он либо прерывается после первого выполнения, либо становится бесконечным
loop=1

errorch=$(grep errorch= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
if [ $errorch = "1" ]; then $patch/sh/check.sh $$; elif [ $errorch = "0" ]; then echo "Проверка конфига не включена"; else
echo "Не верное значение переменной errorch в конфиге"
exit 0; fi

reboot=$(grep reboot= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
upt=$(date=`/bin/date '+%H:%M:%S'` && uptime | sed "s/ $date //" | sed 's/up //' | sed "s/,.*//" | sed s/[^0-9]//g)
if [ $reboot = "0" ] && [ $upt -lt 5 ]; then exit 25; fi

# фикс от проблем при параллельных копиях
srver=$(grep srver= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
scsrch=$(screen -ls | grep $srver | wc -l | sed s/[^0-9]//g)
if [ $scsrch -gt 1 ]; then
echo "многократный запуск, попытка исправления"
killall -s KILL java
killall -s KILL screen
screen -wipe
screen -dmUS $srver bash -c "$patch/start.sh"
exit 0; fi

while [ $loop = "1" ]; do
# инициализация переменных из конфига (обрезка текста после первого =)
auto=$(grep auto= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
rest=$(grep rest= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
param=$(grep param= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
log=$(grep log= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
start=$(grep start= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
pos=$(grep pos= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
sleep=$(grep sleep= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
loop=$(grep loop= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
bkcl=$(grep bkcl= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
bmode=$(grep bmode= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
rstsl=$(grep rstsl= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
kill=$(grep kill= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
zhki=$(grep zhki= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
update=$(grep update= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
upche=$(grep upche= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
cheup=$(grep cheup= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
updown=$(grep updown= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
upwarn=$(grep upwarn= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
wname=$(grep wname= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
lgzp=$(grep lgzp= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
clel=$(grep clel= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
bscr=$(grep bscr= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
cpla=$(grep cpla= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
antistop=$(grep antistop= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
stat=$(grep stat= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
chatlgs=$(grep chatlgs= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
remove=$(grep remove= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
shab=$(grep shab= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
wipe=$(grep wipe= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
timcom=$(grep timcom= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
ames=$(grep ames= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
clstl=$(grep clstl= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
pars=$(grep pars= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
compress=$(grep compress= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
asw=$(grep asw= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
aumes=$(grep aumes= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
autsav=$(grep autsav= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
ticms=$(grep ticms= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
tsto=$(grep tsto= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
cleplas=$(grep cleplas= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
ansto=$(grep ansto= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
intcom=$(grep intcom= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
inscr=$(grep inscr= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
antichat=$(grep antichat= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
guardchat=$(grep guardchat= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
antihack=$(grep antihack= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
anthascr=$(grep anthascr= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
cpsc=$(grep cpsc= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
bsrc=$(grep bsrc= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
srver=$(grep srver= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
scchst=$(grep scchst= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
antprxst=$(grep antprxst= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
antprxof=$(grep antprxof= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
filgua=$(grep filgua= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
serstp=$(grep serstp= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
yascres=$(grep yascres= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
yadisk=$(grep yadisk= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
ysnsc=$(grep ysnsc= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
tempba=$(grep tempba= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
tbascnz=$(grep tbascnz= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
clebalo=$(grep clebalo= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
smart=$(grep smart= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
smtscr=$(grep smtscr= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
nogui=$(grep nogui= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)

if [ $smart = "1" ]; then
screenfix=$(screen -ls | grep $smtscr | wc -m | sed s/[^0-9]//g)
if [ $screenfix -lt 5 ]; then echo "запуск smart.sh" && screen -dmUS $smtscr bash -c "$patch/sh/smart.sh"; fi; fi

if [ $scchst = "1" ]; then
stsccheck=$(screen -ls | grep $srver | wc -m | sed s/[^0-9]//g)
if [ $stsccheck -lt 5 ]; then
echo "запуск не в screen, исправление"
screen -dmUS $srver bash -c "$patch/start.sh" && kill $$; fi; fi

if [ $antistop = "1" ]; then
antspc=$(screen -ls | grep $ansto | wc -m | sed s/[^0-9]//g)
if [ $antspc -lt 5 ]; then
echo "запуск antistoper"
screen -dmUS $ansto bash -c "$patch/sh/antistoper.sh"; fi
antagent=$(ps ux | grep "antistop.sh" | sed /grep/d | wc -c | awk '{print $1-1}')
if [ $antagent -lt 5 ]; then
echo "запуск antistop"
sh $patch/sh/antistop.sh &
disown; fi; fi

if [ $serstp = "1" ]; then echo "отключение не нужных сервисов..." && $patch/sh/serstop.sh; fi

if [ $cheup = "1" ]; then $patch/sh/updater.sh; fi
if [ $update = "1" ] && [ $upche = "0" ] && [ $updown = "0" ]; then $patch/update.sh auto; fi
if [ $upche = "1" ] && [ $update = "0" ] && [ $updown = "0" ]; then $patch/update.sh ch ch; fi
if [ $updown = "1" ] && [ $update = "0" ] && [ $upche = "0" ]; then $patch/update.sh ch up; fi

if [ $pos = "0" ] && [ $auto = "1" ]; then echo "начало выполнения autogen" && $patch/sh/autogen.sh; fi

if [ $clel = "1" ]; then echo "начало чистки логов" && $patch/sh/clearlog.sh; fi
if [ $clstl = "1" ]; then echo "начало удаления стандартных логов" && $patch/sh/clearlog.sh clear; fi
if [ $clebalo = "1" ]; then echo "удаление логов бэкапирования и синхронизации" && $patch/sh/clearlog.sh cleback; fi
if [ $antprxst = "1" ]; then echo "включение анти-прокси" && $patch/sh/antiproxy.sh on; fi

# слип, потому что в скриптах проверка на наличие процесса джавы
# проверки для защиты от повторного запуска скринов
zhcheck=$(screen -ls | grep $tsto | wc -m | sed s/[^0-9]//g)
if [ $zhki = "1" ] && [ $zhcheck -lt 5 ]; then
echo "запуск timestop.sh"
screen -dmUS $tsto bash -c "sleep 120 && $patch/sh/timestop.sh"; fi
timcheck=$(screen -ls | grep $ticms | wc -m | sed s/[^0-9]//g)
if [ $ticms = "1" ] && [ $timcheck -lt 5 ]; then
echo "запуск timecommand.sh"
screen -dmUS $timcomsc bash -c "sleep 120 && $patch/sh/timecommand.sh"; fi
amecheck=$(screen -ls | grep $aumes | wc -m | sed s/[^0-9]//g)
if [ $ames = "1" ] && [ $amecheck -lt 5 ]; then
echo "запуск automessage.sh"
screen -dmUS $aumes bash -c "sleep 120 && $patch/sh/automessage.sh"; fi
ascheck=$(screen -ls | grep $autsav | wc -m | sed s/[^0-9]//g)
if [ $asw = "1" ] && [ $ascheck -lt 5 ]; then
echo "запуск autosave.sh"
screen -dmUS $autsav bash -c "sleep 120 && $patch/sh/autosave.sh"; fi
intcheck=$(screen -ls | grep $inscr | wc -m | sed s/[^0-9]//g)
if [ $intcom = "1" ] && [ $intcheck -lt 5 ]; then
echo "запуск intcom.sh"
screen -dmUS $inscr bash -c "sleep 120 && $patch/sh/intcom.sh"; fi
guacheck=$(screen -ls | grep $antichat | wc -m | sed s/[^0-9]//g)
if [ $guardchat = "1" ] && [ $guacheck -lt 5 ] && [ $log = "1" ]; then
echo "запуск chat.sh"
screen -dmUS $antichat bash -c "sleep 120 && $patch/sh/chat.sh"; fi
anticheck=$(screen -ls | grep $anthascr | wc -m | sed s/[^0-9]//g)
if [ $antihack = "1" ] && [ $anticheck -lt 5 ] && [ $log = "1" ]; then
echo "запуск antihack.sh"
screen -dmUS $anthascr bash -c "sleep 120 && $patch/sh/antihack.sh"; fi
tmpback=$(screen -ls | grep $tbascnz | wc -m | sed s/[^0-9]//g)
if [ $tempba = "1" ] && [ $tmpback -lt 5 ]; then
echo "запуск tempback.sh"
screen -dmUS $tbascnz bash -c "sleep 120 && $patch/sh/tempback.sh"; fi

date=`/bin/date '+%H'`
# защита от повторных срабатываний в тот же день, в час планового рестарта
datech=`/bin/date '+%d.%m.%Y'`
if ! [ -d $patch/temp/back/ ]; then mkdir $patch/temp/back/; fi
if ! [ -f $patch/temp/back/check.cfg ]; then echo "0.0.0" > $patch/temp/back/check.cfg; fi
restho=$(echo $rest | rev | sed -r 's/^[^:]+//' | cut -c 2- | rev)
datecheck=$(head -n 1 $patch/temp/back/check.cfg | tail -n 1)
if [ "$date" = "$restho" ] && [ $bscr = "1" ] && [ "$datecheck" != "$datech" ]; then
echo "запуск бэкапирования в screen"
if [ $bmode = "1" ]; then
screen -dmUS $bsrc bash -c "$patch/back.sh z"
echo $datech > $patch/temp/back/check.cfg; else
screen -dmUS $bsrc bash -c "$patch/back.sh"
echo $datech > $patch/temp/back/check.cfg; fi; fi
# тоже самое с синхронизацией
scrcheck=$(screen -ls | grep $ysnsc | wc -m | sed s/[^0-9]//g)
if ! [ -f $patch/temp/back/sync.cfg ]; then echo "0.0.0" > $patch/temp/back/sync.cfg; fi
syndacheck=$(head -n 1 $patch/temp/back/sync.cfg | tail -n 1)
if [ "$date" = "$restho" ] && [ $yascres = "1" ] && [ $yadisk = "1" ] && [ -f $patch/temp/back/back.cfg ] && [ $scrcheck -lt 5 ] && [ $bscr = "0" ] && [ "$syndacheck" != "$datech" ]; then
backdir=`head -n 1 $patch/temp/back/back.cfg | tail -n 1`
backold=`head -n 2 $patch/temp/back/back.cfg | tail -n 1`
if [ $backold = "none" ]; then
screen -dmUS $ysnsc bash -c "$patch/sh/yasync.sh $backdir"; else screen -dmUS $ysnsc bash -c "$patch/sh/yasync.sh $backdir $backold"; fi
echo $datech > $patch/temp/back/sync.cfg; fi

resthour=$(echo $rest | cut -c -2)
rhour=`/bin/date '+%H'`
if [ $cpsc = "1" ] && [ $cpla = "1" ] && [ $rhour = $resthour ]; then
echo "начало чистки файлов игроков в фоне"
screen -dmUS $cleplas bash -c "$patch/sh/clearplayer.sh"; fi

# отсылаем уведомления, если соответствующие файлы найдены
# они создаются до этого события
if [ $upwarn = "1" ] && [ -f $patch/UPDATE-PL.txt ]; then
echo "Найдены обновления плагинов, проверьте UPDATE-PL.txt"
echo "Отправка уведомления через essentials mail..."
check=`grep mail $patch/plugins/Essentials/userdata/$wname.yml | wc -m | sed s/[^0-9]//g`
if [ $check -gt 3 ]; then
echo "- 'Server: Есть обновления, проверь UPDATE-PL.txt. Список -'" >> $patch/plugins/Essentials/userdata/$wname.yml
while read line; do echo "- 'Server: $line'" >> $patch/plugins/Essentials/userdata/$wname.yml; done < $patch/UPDATE-PL.txt; else
echo "mail:" >> $patch/plugins/Essentials/userdata/$wname.yml
echo "- 'Server: Есть обновления, проверь UPDATE-PL.txt. Список -'" >> $patch/plugins/Essentials/userdata/$wname.yml
while read line; do echo "- 'Server: $line'" >> $patch/plugins/Essentials/userdata/$wname.yml; done < $patch/UPDATE-PL.txt; fi; fi

if [ $upwarn = "1" ] && [ -f $patch/README.txt ]; then
echo "Найдено обновление VOMINATOR, проверьте README.txt"
echo "Отправка уведомления через essentials mail..."
check=`grep mail $patch/plugins/Essentials/userdata/$wname.yml | wc -m | sed s/[^0-9]//g`
if [ $check -gt 3 ]; then
echo "- 'Server: Найдено обновление VOMINATOR, проверьте README.txt'" >> $patch/plugins/Essentials/userdata/$wname.yml; else
echo "mail:" >> $patch/plugins/Essentials/userdata/$wname.yml
echo "- 'Server: Найдено обновление VOMINATOR, проверьте README.txt'" >> $patch/plugins/Essentials/userdata/$wname.yml; fi; fi

cd $patch/

if [ $sleep = "1" ]; then
sleep 5
if [ $kill = "1" ]; then
killall -s KILL java; fi
sleep 2
if [ $kill = "1" ]; then
killall -s KILL java; fi
date=`/bin/date '+%H:%M'`
if [ $rstsl = "1" ] && [ "$date" = "$rest" ]; then
echo "63 секунды до старта"
sleep 60; fi
echo "3 секунды до старта"
sleep 3; fi

if [ $filgua = "1" ]; then $patch/sh/figuard.sh on; fi

# самая главная часть =)
if [ $log = "1" ]; then

echo "запуск сервера с альтернативным логом"
date=`/bin/date '+%H:%M:%S-%d.%m.%Y'`
echo "запуск сервера с альтернативным логом" $date >> $patch/logs/stops.log
# проверям лог на всякий случай, чтобы он не затерся
if [ -f $patch/server.log ]; then
if [ $stat = "1" ]; then $patch/sh/stat.sh; fi
datet=`/bin/date '+%H:%M:%S'`
dateY=`/bin/date '+%Y'`
dateM=`/bin/date '+%m'`
dateD=`/bin/date '+%d'`
if ! [ -d $patch/logs/alt/ ]; then mkdir $patch/logs/alt/; fi
if ! [ -d $patch/logs/alt/$dateY/ ]; then mkdir $patch/logs/alt/$dateY/; fi
if ! [ -d $patch/logs/alt/$dateY/$dateM/ ]; then mkdir $patch/logs/alt/$dateY/$dateM/; fi
if ! [ -d $patch/logs/alt/$dateY/$dateM/$dateD/ ]; then mkdir $patch/logs/alt/$dateY/$dateM/$dateD/; fi
if [ $chatlgs = "1" ] && [ $log = "1" ]; then grep "$shab" $patch/server.log | sed /$remove/d > $patch/chat.log; fi
if [ $pars = "1" ] && [ $log = "1" ]; then $patch/sh/parser.sh; fi
if [ $lgzp = "1" ]; then
zip -$compress $patch/logs/alt/$dateY/$dateM/$dateD/$datet.zip $patch/server.log && rm $patch/server.log
if [ $chatlgs = "1" ] && [ $log = "1" ]; then zip -$compress $patch/logs/alt/$dateY/$dateM/$dateD/$datet-chat.zip $patch/chat.log && rm $patch/chat.log; fi; else
mv $patch/server.log $patch/logs/alt/$dateY/$dateM/$dateD/$datet.log
if [ $chatlgs = "1" ] && [ $log = "1" ]; then mv $patch/chat.log $patch/logs/alt/$dateY/$dateM/$dateD/$datet-chat.log; fi; fi; fi

if [ $nogui = "1" ]; then
java $param -jar $patch/$start nogui | tee server.log; else java $param -jar $patch/$start | tee server.log; fi

if [ $stat = "1" ]; then $patch/sh/stat.sh; fi
# перемещаем лог после выключения
datet=`/bin/date '+%H:%M:%S'`
dateY=`/bin/date '+%Y'`
dateM=`/bin/date '+%m'`
dateD=`/bin/date '+%d'`
if ! [ -d $patch/logs/alt/ ]; then mkdir $patch/logs/alt/; fi
if ! [ -d $patch/logs/alt/$dateY/ ]; then mkdir $patch/logs/alt/$dateY/; fi
if ! [ -d $patch/logs/alt/$dateY/$dateM/ ]; then mkdir $patch/logs/alt/$dateY/$dateM/; fi
if ! [ -d $patch/logs/alt/$dateY/$dateM/$dateD/ ]; then mkdir $patch/logs/alt/$dateY/$dateM/$dateD/; fi
if [ $chatlgs = "1" ] && [ $log = "1" ]; then grep "$shab" $patch/server.log | sed /$remove/d > $patch/chat.log; fi
if [ $pars = "1" ] && [ $log = "1" ]; then $patch/sh/parser.sh; fi
if [ $lgzp = "1" ]; then
zip -$compress $patch/logs/alt/$dateY/$dateM/$dateD/$datet.zip $patch/server.log && rm $patch/server.log
if [ $chatlgs = "1" ] && [ $log = "1" ]; then zip -$compress $patch/logs/alt/$dateY/$dateM/$dateD/$datet-chat.zip $patch/chat.log && rm $patch/chat.log; fi; else
mv $patch/server.log $patch/logs/alt/$dateY/$dateM/$dateD/$datet.log
if [ $chatlgs = "1" ] && [ $log = "1" ]; then mv $patch/chat.log $patch/logs/alt/$dateY/$dateM/$dateD/$datet-chat.log; fi; fi; else

echo "запуск сервера без альтернативного лога"
date=`/bin/date '+%H:%M:%S-%d.%m.%Y'`
echo "запуск сервера без альтернативного лога" $date >> $patch/logs/stops.log
# и тут тоже проверим, на случай смены опции
if [ -f $patch/server.log ]; then
if [ $stat = "1" ]; then $patch/sh/stat.sh; fi
datet=`/bin/date '+%H:%M:%S'`
dateY=`/bin/date '+%Y'`
dateM=`/bin/date '+%m'`
dateD=`/bin/date '+%d'`
if ! [ -d $patch/logs/alt/ ]; then mkdir $patch/logs/alt/; fi
if ! [ -d $patch/logs/alt/$dateY/ ]; then mkdir $patch/logs/alt/$dateY/; fi
if ! [ -d $patch/logs/alt/$dateY/$dateM/ ]; then mkdir $patch/logs/alt/$dateY/$dateM/; fi
if ! [ -d $patch/logs/alt/$dateY/$dateM/$dateD/ ]; then mkdir $patch/logs/alt/$dateY/$dateM/$dateD/; fi
if [ $chatlgs = "1" ] && [ $log = "1" ]; then grep "$shab" $patch/server.log | sed /$remove/d > $patch/chat.log; fi
if [ $pars = "1" ] && [ $log = "1" ]; then $patch/sh/parser.sh; fi
if [ $lgzp = "1" ]; then
zip -$compress $patch/logs/alt/$dateY/$dateM/$dateD/$datet.zip $patch/server.log && rm $patch/server.log
if [ $chatlgs = "1" ] && [ $log = "1" ]; then zip -$compress $patch/logs/alt/$dateY/$dateM/$dateD/$datet-chat.zip $patch/chat.log && rm $patch/chat.log; fi; else
mv $patch/server.log $patch/logs/alt/$dateY/$dateM/$dateD/$datet.log
if [ $chatlgs = "1" ] && [ $log = "1" ]; then mv $patch/chat.log $patch/logs/alt/$dateY/$dateM/$dateD/$datet-chat.log; fi; fi; fi

if [ $nogui = "1" ]; then
java $param -jar $patch/$start nogui; else java $param -jar $patch/$start; fi; fi

# еще небольшая инициализация, на случай смены значений
antprxof=$(grep antprxof= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
filgua=$(grep filgua= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
bscr=$(grep bscr= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
bkcl=$(grep bkcl= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
bmode=$(grep bmode= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
cpla=$(grep cpla= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
wipe=$(grep wipe= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
auto=$(grep auto= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
pos=$(grep pos= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
starstp=$(grep starstp= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
close=$(grep close= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)

date=`/bin/date '+%H:%M'`

if [ $filgua = "1" ]; then $patch/sh/figuard.sh off; fi
if [ $antprxof = "1" ]; then $patch/sh/antiproxy.sh off; fi

if [ "$date" = "$rest" ] && [ $bkcl = "1" ] && [ $bscr = "0" ]; then
if [ $bmode = "1" ]; then $patch/back.sh z; else $patch/back.sh; fi; fi
if [ "$date" = "$rest" ] && [ $wipe = "1" ]; then $patch/sh/wiper.sh; fi
if [ $cpla = "1" ] && [ "$date" = "$rest" ] && [ $cpsc = "0" ]; then $patch/sh/clearplayer.sh; fi
if [ $pos = "1" ] && [ $auto = "1" ]; then $patch/sh/autogen.sh; fi

echo "сервер остановлен"
echo "сервер остановлен" `/bin/date '+%H:%M:%S-%d.%m.%Y'` >> $patch/logs/stops.log

# убиваем антистоп, чтобы не запускать его дважды
if [ $antistop = "1" ]; then
echo "завершение antistop"
aspid=$(head -n 1 $patch/temp/antistop.pid| tail -n 1)
kill -s KILL $aspid >> $patch/logs/antistop.log
rm $patch/temp/antistop.pid >> $patch/logs/antistop.log
aspid=$(head -n 1 $patch/temp/antistoper.pid| tail -n 1)
kill -s KILL $aspid >> $patch/logs/antistop.log
rm $patch/temp/antistoper.pid >> $patch/logs/antistop.log; fi

if [ $close = "1" ] && [ "$date" = "$rest" ]; then echo "перезагрузка" && reboot && exit 0; fi
if [ $starstp = "1" ] && [ "$date" = "$rest" ]; then echo "полное закрытие скрипта" && exit 0; fi

# еще одно считывание переменной, чтобы предотвратить перезапуск в случае изменения параметра
loop=$(grep loop= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-); done
echo "скрипт завершился"