#!/bin/bash
patch=`echo $0 | rev | sed -r 's/^[^/]+//' | cut -c 7- | rev`

apt-get update | tee $patch/temp/upgrade.log
cat $patch/temp/upgrade.log >> $patch/logs/upgrade.log
rm $patch/temp/upgrade.log >> $patch/logs/upgrade.log
apt-get upgrade | tee $patch/temp/upgrade.log
cat $patch/temp/upgrade.log >> $patch/logs/upgrade.log
rm $patch/temp/upgrade.log >> $patch/logs/upgrade.log
apt-get dist-upgrade | tee $patch/temp/upgrade.log
cat $patch/temp/upgrade.log >> $patch/logs/upgrade.log
rm $patch/temp/upgrade.log >> $patch/logs/upgrade.log
apt-get autoremove | tee $patch/temp/upgrade.log
cat $patch/temp/upgrade.log >> $patch/logs/upgrade.log
rm $patch/temp/upgrade.log >> $patch/logs/upgrade.log