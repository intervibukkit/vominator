#!/bin/bash
patch=`echo $0 | rev | sed -r 's/^[^/]+//' | cut -c 5- | rev`

errorch=$(grep errorch= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
if [ $errorch = "1" ]; then $patch/sh/check.sh $$; elif [ $errorch = "0" ]; then echo "Проверка конфига не включена"; else
echo "Не верное значение переменной errorch в конфиге"
exit 0; fi

antiflood=$(grep antiflood= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
prmut=$(grep prmut= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
tflo=$(grep tflo= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
srver=$(grep srver= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
chrtrash=$(grep chrtrash= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
printch=$(grep printch= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
guardchat=$(grep guardchat= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
antiswear=$(grep antiswear= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
swnak=$(grep swnak= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
tswe=$(grep tswe= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
swebro=$(grep swebro= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
antispam=$(grep antispam= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
spabro=$(grep spabro= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
tspa=$(grep tspa= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
spnak=$(grep spnak= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
chrazd=$(grep chrazd= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
nrazd=$(grep nrazd= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
antipundh=$(grep antipundh= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
punak=$(grep punak= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
puntsr=$(grep puntsr= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
punbro=$(grep punbro= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
flnkbro=$(grep flnkbro= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
antichat=$(grep antichat= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)

# защита от двойного запуска
scrcheck=$(screen -ls | grep $antichat | wc -l | sed s/[^0-9]//g)
if [ $scrcheck -gt 1 ]; then
date=`/bin/date '+%H:%M:%S-%d.%m.%Y'`
echo "двойной запуск" $date >> $patch/logs/chat.log
screen -S $antichat -X stuff "^C" >> $patch/logs/chat.log; fi

# главный цикл
while [ $guardchat = "1" ]; do
if ! [ -d $patch/temp/antichat/ ]; then mkdir $patch/temp/antichat/; fi
# если джава не запущена, нет смысла выполнения скрипта
jchek=`ps ux | grep java | sed /grep/d | wc -c | awk '{print $1-1}'`
if ! [ $jchek -gt 1 ]; then
find $patch/temp/antichat/* -type f -name "*log*" -delete
find $patch/temp/antichat/* -type f -name "*txt*" -delete
exit 25; fi

# узнаем номер строки для последующей обрезки, останется то, что накопилось во время паузы
nstr=$(cat $patch/server.log | wc -l | sed s/[^0-9]//g)
sleep $printch

nstr=$(echo $nstr"d")
# а тут сортировка только отправленого в чат по символу-разделителю, чистка от мусора (вдруг сопадет с чем-то еще)
cat $patch/server.log | sed 1,$nstr | grep "$chrazd" | sed /$chrtrash/d > $patch/temp/antichat/antichat.log

# проверка, что кусок лога отрезан и не пустой
chnlog=$(wc -m $patch/temp/antichat/antichat.log | sed s/[^0-9]//g)
if [ $chnlog -gt 3 ]; then
line=$(wc -l $patch/cfg/remchat.cfg | sed s/[^0-9]//g)
step=1
while [ $step != $((line+2)) ]; do
rem=$(head -n $step $patch/cfg/remchat.cfg | tail -n 1)
sed /$rem/d $patch/temp/antichat/antichat.log > $patch/temp/antichat/antichat2.log
mv $patch/temp/antichat/antichat2.log $patch/temp/antichat/antichat.log
step=$((step+1))
# экстренный прерыватель
if [ $step -gt $((line+2)) ]; then step=$(echo $((line+2))) && break; fi; done

# еще одна чистка - убираем строку со временем, чтобы корректно отсортировать повторяющиеся строки (флуд)
line=$(wc -l $patch/temp/antichat/antichat.log | sed s/[^0-9]//g)
step=1
while [ $step != $((line+2)) ]; do
head -n $step $patch/temp/antichat/antichat.log | tail -n 1 | sed -r 's/^[^INFO]+//' | cut -c 8- >> $patch/temp/antichat/antichat2.log
step=$((step+1))
# экстренный прерыватель
if [ $step -gt $((line+2)) ]; then step=$(echo $((line+2))) && break; fi; done
mv $patch/temp/antichat/antichat2.log $patch/temp/antichat/antichat.log

# и поехали по циклам проверки
if [ $antiflood = "1" ]; then
sort $patch/temp/antichat/antichat.log | uniq -d -c | sed 's/^[ \t]*//' > $patch/temp/antichat/flood.log

line=$(wc -l $patch/temp/antichat/flood.log | sed s/[^0-9]//g)
step=1
while [ $step != $((line+2)) ] && [ $line -gt 0 ]; do
mess=$(head -n $step $patch/temp/antichat/flood.log | tail -n 1)
reply=$(echo $mess | awk '{print $1}')
# вырезаем ник, используя разделители, которые нужно поставить в конфиге плагина на чат и указать в global.cfg
nick=$(echo $mess | awk -F[$nrazd] '{print $2}')
nich=$(echo $nick | wc -m | sed s/[^0-9]//g)
flnak=$(grep flnak= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2- | sed "s/%nick%/$nick/g")
if [ -f $patch/temp/antichat/floodnicks.log ]; then nirep=$(grep $nick $patch/temp/antichat/floodnicks.log | wc -m | sed s/[^0-9]//g); else nirep=0; fi
if [ $reply -gt $prmut ] && [ "$nich" -gt 2 ] && [ $nirep = "0" ]; then
date=`/bin/date '+%H:%M:%S-%d.%m.%Y'`
echo $date "---->>" $nick "флудил, выполнение команды" $flnak >> $patch/logs/chat.log
echo "--------------> сообщение:" $mess >> $patch/logs/chat.log
echo $nick >> $patch/temp/antichat/floodnicks.log
if [ $tflo = "0" ]; then
screen -S $srver -X eval "stuff '$flnak'\015"
if [ $flnkbro = "1" ]; then screen -S $srver -X eval "stuff 'broadcast &e&l$nick&r &cфлудил и получил наказание'\015"; fi; fi; fi
step=$((step+1))
# экстренный прерыватель
if [ $step -gt $((line+2)) ]; then step=$(echo $((line+2))) && break; fi; done
rm $patch/temp/antichat/flood.log
if [ -f $patch/temp/antichat/floodnicks.log ]; then rm $patch/temp/antichat/floodnicks.log; fi; fi

if [ $antiswear = "1" ]; then
line=$(wc -l $patch/cfg/antiswear.cfg | sed s/[^0-9]//g)
step=1
while [ $step != $((line+2)) ]; do
swear=$(head -n $step $patch/cfg/antiswear.cfg | tail -n 1)
check=$(grep "$swear" $patch/temp/antichat/antichat.log | wc -m | sed s/[^0-9]//g)
if [ $check -gt 3 ]; then
grep "$swear" $patch/temp/antichat/antichat.log > $patch/temp/antichat/swear.log

line2=$(wc -l $patch/temp/antichat/swear.log | sed s/[^0-9]//g)
step2=1
while [ $step2 != $((line2+2)) ]; do
mess=$(head -n $step2 $patch/temp/antichat/swear.log | tail -n 1)
nick=$(echo $mess | awk -F[$nrazd] '{print $2}')
nich=$(echo $nick | wc -m | sed s/[^0-9]//g)
swnak=$(grep swnak= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2- | sed "s/%nick%/$nick/g")
if [ -f $patch/temp/antichat/swearnicks.log ]; then nirep=$(grep $nick $patch/temp/antichat/swearnicks.log | wc -m | sed s/[^0-9]//g); else nirep=0; fi
date=`/bin/date '+%H:%M:%S-%d.%m.%Y'`
if [ "$nich" -gt 2 ] && [ $nirep = "0" ]; then
echo $date "---->>" $nick "нецензурно выражался, выполнение команды" $swnak >> $patch/logs/chat.log
echo "--------------> сообщение:" $mess >> $patch/logs/chat.log
echo $nick >> $patch/temp/antichat/swearnicks.log
if [ $tswe = "0" ]; then
screen -S $srver -X eval "stuff '$swnak'\015"
if [ $swebro = "1" ]; then screen -S $srver -X eval "stuff 'broadcast &e&l$nick&r &cнецензурно выражался и получил наказание'\015"; fi; fi; fi
step2=$((step2+1))
# экстренный прерыватель
if [ $step -gt $((line+2)) ]; then step=$(echo $((line+2))) && break; fi; done
rm $patch/temp/antichat/swear.log; fi
step=$((step+1)); done
if [ -f $patch/temp/antichat/swearnicks.log ]; then rm $patch/temp/antichat/swearnicks.log; fi; fi

if [ $antipundh = "1" ]; then
line=$(wc -l $patch/cfg/antipund.cfg | sed s/[^0-9]//g)
step=1
while [ $step != $((line+2)) ]; do
pund=$(head -n $step $patch/cfg/antipund.cfg | tail -n 1)
check=$(grep "$pund" $patch/temp/antichat/antichat.log | wc -m | sed s/[^0-9]//g)
if [ $check -gt 3 ]; then
grep "$pund" $patch/temp/antichat/antichat.log > $patch/temp/antichat/pund.log

line2=$(wc -l $patch/temp/antichat/pund.log | sed s/[^0-9]//g)
step2=1
while [ $step2 != $((line2+2)) ]; do
mess=$(head -n $step2 $patch/temp/antichat/pund.log | tail -n 1)
nick=$(echo $mess | awk -F[$nrazd] '{print $2}')
nich=$(echo $nick | wc -m | sed s/[^0-9]//g)
punak=$(grep punak= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2- | sed "s/%nick%/$nick/g")
if [ -f $patch/temp/antichat/pundnicks.log ]; then nirep=$(grep $nick $patch/temp/antichat/pundnicks.log | wc -m | sed s/[^0-9]//g); else nirep=0; fi
date=`/bin/date '+%H:%M:%S-%d.%m.%Y'`
if [ "$nich" -gt 2 ] && [ $nirep = "0" ]; then
echo $date "---->>" $nick "попрашайничел, выполнение команды" $punak >> $patch/logs/chat.log
echo "--------------> сообщение:" $mess >> $patch/logs/chat.log
echo $nick >> $patch/temp/antichat/pundnicks.log
if [ $puntsr = "0" ]; then
screen -S $srver -X eval "stuff '$punak'\015"
if [ $punbro = "1" ]; then screen -S $srver -X eval "stuff 'broadcast &e&l$nick&r &cпопрашайничел и получил наказание'\015"; fi; fi; fi
step2=$((step2+1))
# экстренный прерыватель
if [ $step -gt $((line+2)) ]; then step=$(echo $((line+2))) && break; fi; done
rm $patch/temp/antichat/pund.log; fi
step=$((step+1)); done
if [ -f $patch/temp/antichat/pundnicks.log ]; then rm $patch/temp/antichat/pundnicks.log; fi; fi

if [ $antispam = "1" ]; then
line=$(wc -l $patch/cfg/antispam.cfg | sed s/[^0-9]//g)
step=1
while [ $step != $((line+2)) ]; do
spam=$(head -n $step $patch/cfg/antispam.cfg | tail -n 1)
check=$(grep "$spam" $patch/temp/antichat/antichat.log | wc -m | sed s/[^0-9]//g)
if [ $check -gt 3 ]; then
grep "$spam" $patch/temp/antichat/antichat.log > $patch/temp/antichat/spam.log

line2=$(wc -l $patch/temp/antichat/spam.log | sed s/[^0-9]//g)
step2=1
while [ $step2 != $((line2+2)) ]; do
mess=$(head -n $step $patch/temp/antichat/spam.log | tail -n 1)
nick=$(echo $mess | awk -F[$nrazd] '{print $2}')
nich=$(echo $nick | wc -m | sed s/[^0-9]//g)
spnak=$(grep spnak= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2- | sed "s/%nick%/$nick/g")
if [ -f $patch/temp/antichat/spamnicks.log ]; then nirep=$(grep $nick $patch/temp/antichat/spamnicks.log | wc -m | sed s/[^0-9]//g); else nirep=0; fi
date=`/bin/date '+%H:%M:%S-%d.%m.%Y'`
if [ "$nich" -gt 2 ] && [ $nirep = "0" ]; then
echo $date "---->>" $nick "спамил, выполнение команды" $spnak >> $patch/logs/chat.log
echo "--------------> сообщение:" $mess >> $patch/logs/chat.log
echo $nick >> $patch/temp/antichat/spamnicks.log
if [ $tspa = "0" ]; then
screen -S $srver -X eval "stuff '$spnak'\015"
if [ $spabro = "1" ]; then screen -S $srver -X eval "stuff 'broadcast &e&l$nick&r &cспамил и получил наказание'\015"; fi; fi; fi
step2=$((step2+1))
# экстренный прерыватель
if [ $step2 -gt $((line2+2)) ]; then step2=$(echo $((line2+2))) && break; fi; done
rm $patch/temp/antichat/spam.log; fi
step=$((step+1))
# экстренный прерыватель
if [ $step -gt $((line+2)) ]; then step=$(echo $((line+2))) && break; fi; done
if [ -f $patch/temp/antichat/spamnicks.log ]; then rm $patch/temp/antichat/spamnicks.log; fi; fi; fi

rm $patch/temp/antichat/antichat.log
guardchat=$(grep guardchat= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
antiflood=$(grep antiflood= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
antiswear=$(grep antiswear= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
antispam=$(grep antispam= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
antipundh=$(grep antipundh= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-); done