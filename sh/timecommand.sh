#!/bin/bash
patch=`echo $0 | rev | sed -r 's/^[^/]+//' | cut -c 5- | rev`

errorch=$(grep errorch= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
if [ $errorch = "1" ]; then $patch/sh/check.sh $$; elif [ $errorch = "0" ]; then echo "Проверка конфига не включена"; else
echo "Не верное значение переменной errorch в конфиге"
exit 0; fi

timcom=$(grep timcom= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
srver=$(grep srver= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
ticms=$(grep ticms= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)

# защита от двойного запуска
scrcheck=$(screen -ls | grep $ticms | wc -l | sed s/[^0-9]//g)
if [ $scrcheck -gt 1 ]; then
date=`/bin/date '+%H:%M:%S-%d.%m.%Y'`
echo "двойной запуск timecommand" $date >> $patch/logs/other.log
screen -S $ticms -X stuff "^C" >> $patch/logs/other.log; fi

while [ $timcom = "1" ]; do
jchek=`ps ux | grep java | sed /grep/d | wc -c | awk '{print $1-1}'`
if ! [ $jchek -gt 1 ]; then exit 25; fi

date=`/bin/date '+%H:%M:%S'`
check=$(grep $date $patch/cfg/global.cfg | wc -m | sed s/[^0-9]//g)
if [ $check -gt 2 ]; then
cmd=$(grep $date= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
screen -S $srver -X eval "stuff '$cmd'\015"; fi
sleep 1
timcom=$(grep timcom= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-); done