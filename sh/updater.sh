#!/bin/bash
patch=`echo $0 | rev | sed -r 's/^[^/]+//' | cut -c 5- | rev`

errorch=$(grep errorch= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
if [ $errorch = "1" ]; then $patch/sh/check.sh $$; elif [ $errorch = "0" ]; then echo "Проверка конфига не включена"; else
echo "Не верное значение переменной errorch в конфиге"
exit 0; fi

cheup=$(grep cheup= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
version=$(grep version= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)

if [ $cheup = "1" ]; then
mkdir $patch/temp/check-update/ >> $patch/logs/check-update.log
cd $patch/temp/check-update/ >> $patch/logs/check-update.log
wget -a $patch/logs/check-update.log -c http://bitbucket.org/InterVi/vominator/commits/all
ver=$(head -n $((`grep "hash execute" $patch/temp/check-update/all -n | rev | sed -r 's/^[^:]+//' | cut -c 2- | rev | head -n 1 | sed s/[^0-9]//g` + 1)) $patch/temp/check-update/all | tail -n 1 | sed -r 's/^[^>]+//' | cut -c 2- | rev | sed -r 's/^[^<]+//' | cut -c 2- | rev)
if [ $ver != $version ]; then
echo "Новая версия:" $ver >> $patch/logs/check-update.log
echo "Пора обновлятся: https://bitbucket.org/InterVi/vominator/commits/all" > $patch/README-UPDATE-VOMINATOR.txt
echo "Найдено обновление!"
echo "Проверьте README-UPDATE-VOMINATOR.txt в папке сервера!"; fi
rm -rf $patch/temp/check-update/ >> $patch/logs/check-update.log; fi