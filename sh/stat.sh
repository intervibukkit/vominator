#!/bin/bash
patch=`echo $0 | rev | sed -r 's/^[^/]+//' | cut -c 5- | rev`

errorch=$(grep errorch= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
if [ $errorch = "1" ]; then $patch/sh/check.sh $$; elif [ $errorch = "0" ]; then echo "Проверка конфига не включена"; else
echo "Не верное значение переменной errorch в конфиге"
exit 0; fi

if [ -f $patch/server.log ]; then
# костыль для поддержки RainBow
rainche=$(grep "This server is running Rainbow version" $patch/server.log | wc -m | sed s/[^0-9]//g)
if [ $rainche -gt 5 ] && [ -f $patch/Rainbow.properties ]; then
grep " -- LOGIN -- " $patch/server.log | sed -r 's/^[^)]+//' | cut -c 2- | sed -r 's/^[^:]+//' | cut -c 2- | sed -r 's/^[^ ]+//' | cut -c 2- | sed -r 's/^[^ ]+//' | cut -c 2- | rev | sed -r 's/^[^ ]+//' | cut -c 2- | rev | sort -u > $patch/temp/ip.txt
grep " -- LOGIN -- " $patch/server.log | sed -r 's/^[^-]+//' | cut -c 2- | sed -r 's/^[^-]+//' | cut -c 2- | sed -r 's/^[^-]+//' | cut -c 2- | sed -r 's/^[^ ]+//' | cut -c 2- | rev | sed -r 's/^[^(]+//' | cut -c 2- | sed -r 's/^[^ ]+//' | cut -c 2- | sed -r 's/^[^ ]+//' | cut -c 2- | rev | sort -u > $patch/temp/nick.txt; else
grep "logged in with entity id" $patch/server.log | sed -r 's/^[^/]+//' | cut -c 2- | rev | sed -r 's/^[^:]+//' | cut -c 2- | rev | sort -u > $patch/temp/ip.txt
grep "logged in with entity id" $patch/server.log | sed -r 's/^[^INFO]+//' | cut -c 8- | rev | sed -r 's/^[^/]+//' | cut -c 3- | rev | sort -u > $patch/temp/nick.txt; fi
grep "registered" $patch/server.log > $patch/temp/reg.txt
ip=$(wc -l $patch/temp/ip.txt | sed s/[^0-9]//g)
nick=$(wc -l $patch/temp/nick.txt | sed s/[^0-9]//g)
reg=$(wc -l $patch/temp/reg.txt | sed s/[^0-9]//g)
rm $patch/temp/ip.txt
rm $patch/temp/nick.txt
rm $patch/temp/reg.txt
if [ -f $patch/plugins/AuthMe/auths.db ]; then all=$(wc -l $patch/plugins/AuthMe/auths.db | sed s/[^0-9]//g) && all=$((all-1)); fi
date=`/bin/date '+%H:%M:%S-%d.%m.%Y'`
echo "========================" $date "========================" >> $patch/logs/statistic.log
if [ $all ]; then echo $all "зарегистрированных игроков" >> $patch/logs/statistic.log; fi
echo $ip "уникальных ip" >> $patch/logs/statistic.log
echo $nick "уникальных ников" >> $patch/logs/statistic.log
echo $reg "новых игроков" >> $patch/logs/statistic.log
echo "========================" $date "========================"
if [ $all ]; then echo $all "зарегистрированных игроков"; fi
echo $ip "уникальных ip"
echo $nick "уникальных ников"
echo $reg "новых игроков"; else echo "лог server.log не найден"; fi