#!/bin/bash
patch=`echo $0 | rev | sed -r 's/^[^/]+//' | cut -c 5- | rev`

errorch=$(grep errorch= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
if [ $errorch = "1" ]; then $patch/sh/check.sh $$; elif [ $errorch = "0" ]; then echo "Проверка конфига не включена"; else
echo "Не верное значение переменной errorch в конфиге"
exit 0; fi

check=$(echo "$1" | wc -m | sed s/[^0-9]//g)
if [ $check -gt 2 ]; then
if [ "$1" = "on" ]; then
step=1
line=$(wc -l $patch/cfg/files-guard.cfg | sed s/[^0-9]//g)
while [ $((line+2)) != $step ]; do
file=$(head -n $step $patch/cfg/files-guard.cfg | tail -n 1)
chattr +i $patch/$file
step=$((step+1)); done
echo "файлы защищены от редактирования"; elif [ "$1" = "off" ]; then
step=1
line=$(wc -l $patch/cfg/files-guard.cfg | sed s/[^0-9]//g)
while [ $((line+2)) != $step ]; do
file=$(head -n $step $patch/cfg/files-guard.cfg | tail -n 1)
chattr -i $patch/$file
step=$((step+1)); done
echo "файлы доступны для редактирования"; else echo "не верный ввод" && exit 0; fi; fi