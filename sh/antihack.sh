#!/bin/bash
patch=`echo $0 | rev | sed -r 's/^[^/]+//' | cut -c 5- | rev`

errorch=$(grep errorch= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
if [ $errorch = "1" ]; then $patch/sh/check.sh $$; elif [ $errorch = "0" ]; then echo "Проверка конфига не включена"; else
echo "Не верное значение переменной errorch в конфиге"
exit 0; fi

antihack=$(grep antihack= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
hsrt=$(grep hsrt= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
natsrt=$(grep natsrt= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
tesrha=$(grep tesrha= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
vzlnbro=$(grep vzlnbro= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
hachint=$(grep hachint= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
breip=$(grep breip= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
ipwrong=$(grep ipwrong= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
cmatt=$(grep cmatt= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
ipernik=$(grep ipernik= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
tswripb=$(grep tswripb= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
srver=$(grep srver= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
anthascr=$(grep anthascr= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)

# защита от двойного запуска
scrcheck=$(screen -ls | grep $anthascr | wc -l | sed s/[^0-9]//g)
if [ $scrcheck -gt 1 ]; then
date=`/bin/date '+%H:%M:%S-%d.%m.%Y'`
echo "двойной запуск" $date >> $patch/logs/antihack.log
screen -S $anthascr -X stuff "^C" >> $patch/logs/antihack.log; fi

while [ $antihack = "1" ]; do
if ! [ -d $patch/temp/antihack/ ]; then mkdir $patch/temp/antihack/; fi
# если джава не запущена, нет смысла выполнения скрипта
jchek=`ps ux | grep java | sed /grep/d | wc -c | awk '{print $1-1}'`
if ! [ $jchek -gt 1 ]; then
find $patch/temp/antihack/* -type f -name "*log*" -delete
find $patch/temp/antihack/* -type f -name "*txt*" -delete
exit 25; fi

# узнаем номер строки для последующей обрезки, останется то, что накопилось во время паузы
nstr=$(cat $patch/server.log | wc -l | sed s/[^0-9]//g)
sleep $((hachint*60))

nstr=$(echo $nstr"d")
cat $patch/server.log | sed 1,$nstr > $patch/temp/antihack/antihack2.log
check=$(cat $patch/temp/antihack/antihack2.log | grep "used the wrong password" | wc -m | sed s/[^0-9]//g)
if [ $check -gt 3 ]; then
cat $patch/temp/antihack/antihack2.log | grep "used the wrong password" > $patch/temp/antihack/antihack.log

line=$(wc -l $patch/temp/antihack/antihack.log | sed s/[^0-9]//g)
if [ $line -gt $hsrt ]; then
if [ $breip = "1" ]; then
# и много много анализа логов...
grep "logged in with entity id" $patch/temp/antihack/antihack2.log | sed -r 's/^[^/]+//' | cut -c 2- | rev | sed -r 's/^[^:]+//' | cut -c 2- | rev > $patch/temp/antihack/iphack.txt
sort $patch/temp/antihack/iphack.txt | uniq -d -c | sed 's/^[ \t]*//' > $patch/temp/antihack/iphack3.txt
mv $patch/temp/antihack/iphack3.txt $patch/temp/antihack/iphack.txt
line2=$(wc -l $patch/temp/antihack/iphack.txt | sed s/[^0-9]//g)
step2=1
while [ $step2 != $((line2+1)) ]; do
iprep=$(head -n $step2 $patch/temp/antihack/iphack.txt | tail -n 1)
ip=$(echo $iprep| sed -r 's/^[^ ]+//' | cut -c 2-)
wrls=$(echo $iprep | rev | sed -r 's/^[^ ]+//' | cut -c 2- | rev)

grep $ip $patch/temp/antihack/antihack2.log | sed -r 's/^[^INFO]+//' | cut -c 8- | rev | sed -r 's/^[^/]+//' | cut -c 3- | rev > $patch/temp/antihack/ippernick.log
ippernick=$(wc -l $patch/temp/antihack/ippernick.log | sed s/[^0-9]//g)
rm $patch/temp/antihack/ippernick.log

if [ $wrls -gt $ipwrong ] && [ $ippernick -gt $ipernik ]; then
date=`/bin/date '+%H:%M:%S-%d.%m.%Y'`
echo $date "---->>" $ip "осуществил подбор пароля" $wrls "раз, за" $ippernick "ников, в бан"  >> $patch/logs/antihack.log
if [ $tswripb = "0" ]; then iptables -I INPUT -s $ip -j DROP; fi; fi
step2=$((step2+1))
# экстренный прерыватель
if [ $step2 -gt $((line2+1)) ]; then step2=$(echo $((line2+2))) && break; fi; done
rm $patch/temp/antihack/iphack.txt; fi

if [ $cmatt = "1" ]; then
step=1
while [ $step != $((line+1)) ]; do
mess=$(head -n $step $patch/temp/antihack/antihack.log | tail -n 1)
nick=$(echo $mess | sed -r 's/^[^AuthMe]+//' | cut -c 9- | rev | sed -r 's/^[^ ]+//' | cut -c 2- | sed -r 's/^[^ ]+//' | cut -c 2- | sed -r 's/^[^ ]+//' | cut -c 2- | sed -r 's/^[^ ]+//' | cut -c 2- | rev)
grep $nick $patch/temp/antihack/antihack.log > $patch/temp/antihack/$nick.log
att=$(wc -l $patch/temp/antihack/$nick.log | sed s/[^0-9]//g)
vznak=$(grep vznak= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2- | sed "s/%nick%/$nick/g")

if [ $att -gt $natsrt ]; then
date=`/bin/date '+%H:%M:%S-%d.%m.%Y'`
echo $date "---->>" $nick "взламывают, выполнение команды" $vznak >> $patch/logs/antihack.log
if [ $tesrha = "0" ]; then
screen -S $srver -X eval "stuff '$vznak'\015"
if [ $vzlnbro = "1" ]; then screen -S $srver -X eval "stuff 'broadcast &e&l$nick&r &cвзламывают'\015"; fi; fi; fi
rm $patch/temp/antihack/$nick.log
step=$((step+1))
# экстренный прерыватель
if [ $step -gt $((line+1)) ]; then step=$(echo $((line+2))) && break; fi; done; fi; fi; fi

if [ -f $patch/temp/antihack/antihack.log ]; then rm $patch/temp/antihack/antihack.log; fi
rm $patch/temp/antihack/antihack2.log
antihack=$(grep antihack= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
breip=$(grep breip= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
cmatt=$(grep cmatt= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-); done