#!/bin/bash
patch=`echo $0 | rev | sed -r 's/^[^/]+//' | cut -c 5- | rev`
step=1

line=$(wc -l $patch/sh/perem.cfg | sed s/[^0-9]//g)

# чтобы не запускалось много раз из start.sh
if [ -f $patch/temp/checker.cfg ]; then
hcc=`/bin/date '+%H'`
mcc=`/bin/date '+%M'`
hcheck=$(head -n 1 $patch/temp/checker.cfg | tail -n 1)
mcheck=$(head -n 2 $patch/temp/checker.cfg | tail -n 1); fi

if [ $hcc != $hcheck ] || [ $mcc != $mcheck ] || ! [ -f $patch/temp/checker.cfg ]; then
date=`/bin/date '+%H:%M:%S-%d.%m.%Y'`
echo "начало проверки:" $date >> $patch/logs/check.log
while [ $step != $((line+2)) ]; do
perem=`head -n $step $patch/sh/perem.cfg | tail -n 1`
perem2=$(echo $perem)
name=`grep $perem= $patch/cfg/global.cfg | rev | sed -r 's/^[^=]+//' | cut -c 2- | rev`
perem=`grep $perem= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-`
check=`echo $perem | wc -c | awk '{print $1-1}'`
if [ $check = "0" ]; then
echo "Переменная" $perem2 "пуста, проверьте конфиг"
echo "Переменная" $perem2 "пуста, проверьте конфиг" >> $patch/logs/check.log
kill $1
kill $$; fi
if [ $perem = "0" ]; then
echo "верное значение переменной" $name":" $perem >> $patch/logs/check.log; elif [ $perem = "1" ]; then
echo "верное значение переменной" $name":" $perem >> $patch/logs/check.log; else
echo "не верное значение переменной" $name":" $perem >> $patch/logs/check.log
echo "не верное значение переменной" $name":" $perem
kill $1
kill $$; fi
step=$((step+1)); done

step=1

line=$(wc -l $patch/sh/perem2.cfg | sed s/[^0-9]//g)
while [ $step != $((line+2)) ]; do
perem=`head -n $step $patch/sh/perem2.cfg | tail -n 1`
perem2=$(echo $perem)
name=`grep $perem= $patch/cfg/global.cfg | rev | sed -r 's/^[^=]+//' | cut -c 2- | rev`
perem=`grep $perem= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-`
check=`echo $perem | wc -c | awk '{print $1-1}'`
if [ $check = "0" ]; then
echo "Переменная" $perem2 "пуста, проверьте конфиг" >> $patch/logs/check.log
echo "Переменная" $perem2 "пуста, проверьте конфиг"
kill $1
kill $$; fi
check=`echo $perem | sed s/[^0-9]//g`
if [ $check -lt "1" ]; then
echo "Переменная" $name "имеет слишком малое значение, проверьте конфиг"
echo "Переменная" $name "имеет слишком малое значение, проверьте конфиг" >> $patch/logs/check.log
kill $1
kill $$; fi
check=`echo $perem | tr -d [:digit:] | wc -m | sed s/[^0-9]//g`
if [ $check -gt "1" ]; then
echo "Переменная" $name "имеет не допустимое значение, проверьте конфиг"
echo "Переменная" $name "имеет не допустимое значение, проверьте конфиг" >> $patch/logs/check.log
kill $1
kill $$; fi
step=$((step+1)); done

step=1

line=$(wc -l $patch/sh/perem3.cfg | sed s/[^0-9]//g)
while [ $step != $((line+2)) ]; do
perem=`head -n $step $patch/sh/perem3.cfg | tail -n 1`
perem2=$(echo $perem)
name=`grep $perem= $patch/cfg/global.cfg | rev | sed -r 's/^[^=]+//' | cut -c 2- | rev`
perem=`grep $perem= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-`
check=`echo $perem | wc -c | awk '{print $1-1}'`
if [ $check = "0" ]; then
echo "Переменная" $perem2 "пуста, проверьте конфиг"
echo "Переменная" $perem2 "пуста, проверьте конфиг" >> $patch/logs/check.log
kill $1
kill $$; fi
if [ $check -lt "1" ]; then
echo "Переменная" $name "слишком короткая, проверьте конфиг"
echo "Переменная" $name "слишком короткая, проверьте конфиг" >> $patch/logs/check.log
kill $1
kill $$; fi
step=$((step+1)); done

hcc=`/bin/date '+%H'`
mcc=`/bin/date '+%M'`
echo $hcc > $patch/temp/checker.cfg
echo $mcc >> $patch/temp/checker.cfg
echo "проверка global.cfg завершена"; fi