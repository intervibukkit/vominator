#!/bin/bash
patch=`echo $0 | rev | sed -r 's/^[^/]+//' | cut -c 5- | rev`

errorch=$(grep errorch= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
if [ $errorch = "1" ]; then $patch/sh/check.sh $$; elif [ $errorch = "0" ]; then echo "Проверка конфига не включена"; else
echo "Не верное значение переменной errorch в конфиге"
exit 0; fi

date=`/bin/date '+%H:%M:%S-%d.%m.%Y'`
echo $date "- запуск kill.sh, убиваем java" >> $patch/logs/stops.log
killall -s KILL java >> $patch/logs/antistop.log
sleep 5
killall -s KILL java >> $patch/logs/antistop.log