#!/bin/bash
patch=`echo $0 | rev | sed -r 's/^[^/]+//' | cut -c 5- | rev`

errorch=$(grep errorch= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
if [ $errorch = "1" ]; then $patch/sh/check.sh $$; elif [ $errorch = "0" ]; then echo "Проверка конфига не включена"; else
echo "Не верное значение переменной errorch в конфиге"
exit 0; fi

check=$(echo "$1" | wc -m | sed s/[^0-9]//g)
if [ $check -lt 2 ]; then
echo "IP Proxy собирать в" $patch/cfg/proxy.cfg "без лишних табуляций и знаков"
echo "Заблокировать IP из списка?"
echo "on - заблокировать, off - разблокировать"
read who
if [ $who = "on" ]; then echo "Старт блокировки..."; elif [ $who = "off" ]; then echo "Старт разблокировки..."; else echo "Не верный ответ" && exit 25; fi; fi

if [ "$1" != "on" ] && [ "$1" != "off" ]; then
step=1
line=$(wc -l $patch/cfg/proxy.cfg | sed s/[^0-9]//g)
while [ $((line+2)) != $step ]; do
ip=$(head -n $step $patch/cfg/proxy.cfg | tail -n 1)
if [ $who = "on" ]; then
iptables -I INPUT -s $ip -j DROP; else
iptables -D INPUT -s $ip -j DROP; fi
step=$((step+1)); done; fi

step=1
line=$(wc -l $patch/cfg/proxy.cfg | sed s/[^0-9]//g)
while [ $((line+2)) != $step ]; do
ip=$(head -n $step $patch/cfg/proxy.cfg | tail -n 1)
if [ "$1" = "on" ]; then
iptables -I INPUT -s $ip -j DROP; elif [ "$1" = "off" ]; then
iptables -D INPUT -s $ip -j DROP; else echo "Не верный ввод" && exit 25; fi
step=$((step+1)); done

echo "Все готово"