#!/bin/bash
patch=`echo $0 | rev | sed -r 's/^[^/]+//' | cut -c 5- | rev`

errorch=$(grep errorch= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
if [ $errorch = "1" ]; then $patch/sh/check.sh $$; elif [ $errorch = "0" ]; then echo "Проверка конфига не включена"; else
echo "Не верное значение переменной errorch в конфиге"
exit 0; fi

ames=$(grep ames= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
sec=$(grep sec= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
srver=$(grep srver= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
aumes=$(grep aumes= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)

# защита от двойного запуска
scrcheck=$(screen -ls | grep $aumes | wc -l | sed s/[^0-9]//g)
if [ $scrcheck -gt 1 ]; then
date=`/bin/date '+%H:%M:%S-%d.%m.%Y'`
echo "двойной запуск automessage" $date >> $patch/logs/other.log
screen -S $aumes -X stuff "^C" >> $patch/logs/other.log; fi

step=1
line=$(wc -l $patch/cfg/automessage.cfg | sed s/[^0-9]//g)
while [ $ames = "1" ] && [ $line != 0 ]; do
jchek=`ps ux | grep java | sed /grep/d | wc -c | awk '{print $1-1}'`
if ! [ $jchek -gt 1 ]; then exit 25; fi

sleep $sec
mess=$(head -n $step $patch/cfg/automessage.cfg | tail -n 1)
screen -S $srver -X eval "stuff 'broadcast $mess'\015"
if [ $line = $step ]; then step=1; else step=$((step+1)); fi
ames=$(grep ames= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
sec=$(grep sec= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-); done