#!/bin/bash
patch=`echo $0 | rev | sed -r 's/^[^/]+//' | cut -c 5- | rev`

errorch=$(grep errorch= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
if [ $errorch = "1" ]; then $patch/sh/check.sh $$; elif [ $errorch = "0" ]; then echo "Проверка конфига не включена"; else
echo "Не верное значение переменной errorch в конфиге"
exit 0; fi

pars=$(grep pars= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
log=$(grep log= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)

check=$(echo "$1" | wc -m | sed s/[^0-9]//g)
if [ $check -gt 2 ] && [ $log = "1" ]; then
date=`/bin/date '+%H:%M:%S-%d.%m.%Y'`
echo "=================" $date "=================" >> $patch/pars.txt
echo "--->====" $1 "====" >> $patch/pars.txt
echo "-------" server.log "-------" >> $patch/pars.txt
chl=$(grep "$1" $patch/server.log | wc -m | sed s/[^0-9]//g)
if [ $chl -gt 5 ]; then
echo "парсинг из" $patch/server.log
grep "$1" $patch/server.log >> $patch/pars.txt; else echo "ничего не найдено" >> $patch/pars.txt; fi; fi

if [ $check -gt 2 ] && [ $log = "1" ] && [ "$2" = "all" ]; then
if ! [ -d $patch/temp/zips/ ]; then mkdir $patch/temp/zips/; else rm -rf $patch/temp/zips/*; fi
cd $patch/logs/alt/
# основной цикл
fold=$(find . -type d | wc -l | sed s/[^0-9]//g)
find . -type d > $patch/temp/folder.cfg
step=1
while [ $((fold+1)) != $step ]; do
folder=$(head -n $step $patch/temp/folder.cfg | tail -n 1)
cd $patch/logs/alt/$folder/
# поиск по архивам
arch=$(find . -type f -name "*.zip" | wc -l | sed s/[^0-9]//g)
find . -type f -name "*.zip" > $patch/temp/arch.cfg
if [ $arch != "0" ]; then
echo "поиск по логам в архивах"
step2=1
while [ $((arch+1)) != $step2 ]; do
rm -rf $patch/temp/zips/*
file=$(head -n $step2 $patch/temp/arch.cfg | tail -n 1)
# защита от распаковки архивов с логом чата
chach=$(echo `expr "$file" : '.*\(chat\)' | wc -m | sed s/[^0-9]//g`)
if ! [ $chach -gt 2 ]; then
echo "разорхивирование" $patch/logs/alt/$folder/$file
log=$(unzip -d $patch/temp/zips/ $patch/logs/alt/$folder/$file | grep inflating | tail -n 2 | sed -r 's/^[^/]+//' | cut -c 2- | awk '{print "/"$0}')
# защита от пустых срабатываний и от пустого пути
logche=$(echo $log | wc -m | sed s/[^0-9]//g)
if [ "$logche" -gt 2 ]; then
chlogs=$(grep "$1" $log | wc -m | sed s/[^0-9]//g); else chlogs=0; fi
if [ $chlogs -gt 5 ]; then
echo "-------" $folder - $file "-------" >> $patch/pars.txt
echo "парсинг из" $log
grep "$1" $log >> $patch/pars.txt; fi; fi
step2=$((step2+1)); done
rm -rf $patch/temp/zips/*; fi
# поиск по логам
logs=$(find . -type f -name "*.log" | wc -l | sed s/[^0-9]//g)
find . -type f -name "*.log" > $patch/temp/logs.cfg
if [ $logs != "0" ]; then
echo "поиск по логам"
step3=1
while [ $((logs+1)) != $step3 ]; do
log=$(head -n $step3 $patch/temp/logs.cfg | tail -n 1)
echo "поиск в" $patch/logs/alt/$folder/$log
chlogs=$(grep "$1" $patch/logs/alt/$folder/$log | wc -m | sed s/[^0-9]//g)
if [ $chlogs -gt 5 ]; then
echo "-------" $folder - $file "-------" >> $patch/pars.txt
echo "парсинг из" $patch/logs/alt/$folder/$log
grep "$1" $patch/logs/alt/$folder/$log >> $patch/pars.txt; fi
step3=$((step3+1)); done; fi
rm $patch/temp/arch.cfg
rm $patch/temp/logs.cfg
step=$((step+1)); done
rm -rf $patch/temp/zips/
rm $patch/temp/folder.cfg; fi

if [ $pars = "1" ] && [ $log = "1" ] && [ $check -lt 2 ]; then
date=`/bin/date '+%H:%M:%S-%d.%m.%Y'`
echo "=================" $date "=================" >> $patch/logs/pars.log
line=$(wc -l $patch/cfg/pars.cfg | sed s/[^0-9]//g)
step=1
while [ $((line+2)) != $step ]; do
req=`head -n $step $patch/cfg/pars.cfg | tail -n 1`
chlogs=$(grep "$req" $patch/server.log | wc -m | sed s/[^0-9]//g)
if [ $chlogs -gt 5 ]; then
echo "--->====" $req "====" >> $patch/logs/pars.log
echo "парсинг из" $patch/server.log
grep "$req" $patch/server.log >> $patch/logs/pars.log; fi
step=$((step+1)); done; fi