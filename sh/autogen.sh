#!/bin/bash
patch=`echo $0 | rev | sed -r 's/^[^/]+//' | cut -c 5- | rev`
check=$(head -n 1 $patch/sh/auto.cfg | tail -n 1)

errorch=$(grep errorch= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
if [ $errorch = "1" ]; then $patch/sh/check.sh $$; elif [ $errorch = "0" ]; then echo "Проверка конфига не включена"; else
echo "Не верное значение переменной errorch в конфиге"
exit 0; fi

play=$(grep play= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
auto=$(grep auto= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)

if [ $check -lt $play ] && [ $auto = "1" ]; then
cp $patch/cfg/autogen.cfg $patch/sh/auto.sh
chmod 777 $patch/sh/auto.sh
check=$((check+1))
echo $check > $patch/sh/auto.cfg
$patch/sh/auto.sh >> $patch/logs/autogen.log
rm $patch/sh/auto.sh; fi

if [ $check = $play ] && [ $auto = "1" ]; then
echo "0" > $patch/sh/auto.cfg
sed 's/^auto=.*/auto=0/g' $patch/cfg/global.cfg > $patch/sh/global2.cfg
cp $patch/cfg/global.cfg $patch/cfg/global.cfg.old
mv $patch/sh/global2.cfg $patch/cfg/global.cfg; fi