#!/bin/bash
patch=`echo $0 | rev | sed -r 's/^[^/]+//' | cut -c 5- | rev`

errorch=$(grep errorch= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
if [ $errorch = "1" ]; then $patch/sh/check.sh $$; elif [ $errorch = "0" ]; then echo "Проверка конфига не включена"; else
echo "Не верное значение переменной errorch в конфиге"
exit 0; fi

dwstep=$(grep dwstep= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
wpm=$(grep wpm= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
wipe=$(grep wipe= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)

step=$(head -n 1 $patch/sh/wiper.cfg | tail -n 1)

# защита от краша
check=$(ps -aux | grep "java" | wc -m | sed s/[^0-9]//g)
whste=0
while [ $check -lt 4 ]; do
sleep 10
check=$(ps -aux | grep "java" | wc -m | sed s/[^0-9]//g)
whste=$((whste+1))
if [ $whste = "30" ]; then exit 0; fi; done

# защита на случай отключения и повтороного включения опции
# подразумевается, что при включенной опции файл wiper.cfg каждый день редактируется
dchange=`stat -c '%y' $patch/sh/wiper.cfg | cut -c -10`
y=`echo $dchange | cut -c -4`
m=`echo $dchange | cut -c 6- | cut -c -2`
mch=`echo $m | cut -c -1` && if [ $mch = "0" ]; then m=`echo $m | cut -c 2-`; fi
d=`echo $dchange | cut -c 9-`
dch=`echo $d | cut -c -1` && if [ $dch = "0" ]; then d=`echo $d | cut -c 2-`; fi
dy=`/bin/date '+%Y'`
dm=`/bin/date '+%m'`
dmch=`echo $dm | cut -c -1` && if [ $dmch = "0" ]; then dm=`echo $dm | cut -c 2-`; fi
dday=`/bin/date '+%d'`
ddmch=`echo $dday | cut -c -1` && if [ $ddmch = "0" ]; then dday=`echo $dday | cut -c 2-`; fi
day=0
if [ $y != $dy ]; then dat=$((dy-y)) && dat=$((dat*360)) && day=`echo $dat`; fi
if [ $m != $dm ]; then dat=$((dm-m)) && dat=$((dat*30)) && day=$((day+dat)); fi
if [ $d != $dday ]; then dat=$((dday-d)) && day=$((day+dat)); fi
if [ $day -gt 1 ]; then echo 1 > $patch/sh/wiper.cfg; fi

# двойные проверки переменной во многих скриптах, запускаемых из start.sh
# уберегают от ошибок в случае их изменения в конфиге (почти все переменные инициализируются 1 раз при старте)
if ! [ -d $patch/$wpm/ ]; then echo "нет такой папки" $patch/$wpm/ && echo "нет такой папки" $patch/$wpm/ >> $patch/logs/wipe.log && exit 0; fi
if [ $step = $dwstep ] && [ $wipe = "1" ]; then
date=$(/bin/date '+%H:%M:%S-%d.%m.%Y')
echo "Вайп карты" $wpm: $date
echo "Вайп карты" $wpm: $date >> $patch/logs/wipe.log
rm -rf $patch/$wpm/ >> $patch/logs/wipe.log
echo 1 > $patch/sh/wiper.cfg; else
step=$((step+1))
echo $step > $patch/sh/wiper.cfg; fi