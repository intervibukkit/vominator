#!/bin/bash
patch=`echo $0 | rev | sed -r 's/^[^/]+//' | cut -c 5- | rev`

errorch=$(grep errorch= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
if [ $errorch = "1" ]; then $patch/sh/check.sh $$; elif [ $errorch = "0" ]; then echo "Проверка конфига не включена"; else
echo "Не верное значение переменной errorch в конфиге"
exit 0; fi

zhki=$(grep zhki= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
rest=$(grep rest= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
srver=$(grep srver= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
reason=$(grep reason= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
kilsto=$(grep kilsto= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
conttimer=$(grep conttimer= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
tsto=$(grep tsto= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)

# защита от двойного запуска
scrcheck=$(screen -ls | grep $tsto | wc -l | sed s/[^0-9]//g)
if [ $scrcheck -gt 1 ]; then
date=`/bin/date '+%H:%M:%S-%d.%m.%Y'`
echo "двойной запуск timestop" $date >> $patch/logs/other.log
screen -S $tsto -X stuff "^C" >> $patch/logs/other.log; fi

while [ $zhki = "1" ] ; do
date=`/bin/date '+%H:%M'`
if [ $date = $rest ]; then
screen -S $srver -X eval "stuff 'say --------< ВНИМАНИЕ!!! >--------'\015"
screen -S $srver -X eval "stuff 'say ПЕРЕЗАГРУЗКА СЕРВЕРА ЧЕРЕЗ 5 СЕКУНД!'\015"
sleep 5
screen -S $srver -X eval "stuff 'save-all'\015"
screen -S $srver -X eval "stuff 'kickall $reason'\015"
sleep 5
screen -S $srver -X eval "stuff 'save-all'\015"
screen -S $srver -X eval "stuff 'stop'\015"
if [ $kilsto = "1" ]; then
sleep $conttimer && killall -s KILL java
sleep 4 && killall -s KILL java; fi
exit 0; fi

jchek=`ps ux | grep java | sed /grep/d | wc -c | awk '{print $1-1}'`
if ! [ $jchek -gt 1 ]; then exit 25; fi

sleep 10
zhki=$(grep zhki= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
rest=$(grep rest= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
server=$(grep server= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
contkill=$(grep contkill= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-); done
