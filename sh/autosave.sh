#!/bin/bash
patch=`echo $0 | rev | sed -r 's/^[^/]+//' | cut -c 5- | rev`

errorch=$(grep errorch= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
if [ $errorch = "1" ]; then $patch/sh/check.sh $$; elif [ $errorch = "0" ]; then echo "Проверка конфига не включена"; else
echo "Не верное значение переменной errorch в конфиге"
exit 0; fi

asw=$(grep asw= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
intsav=$(grep intsav= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
metod=$(grep metod= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
brsav=$(grep brsav= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
srver=$(grep srver= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
autsav=$(grep autsav= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)

# защита от двойного запуска
scrcheck=$(screen -ls | grep $autsav | wc -l | sed s/[^0-9]//g)
if [ $scrcheck -gt 1 ]; then
date=`/bin/date '+%H:%M:%S-%d.%m.%Y'`
echo "двойной запуск autosave" $date >> $patch/logs/other.log
screen -S $autsav -X stuff "^C" >> $patch/logs/other.log; fi

while [ $asw = "1" ]; do
jchek=`ps ux | grep java | sed /grep/d | wc -c | awk '{print $1-1}'`
if ! [ $jchek -gt 1 ]; then exit 25; fi

sleep $intsav
if [ $brsav = "1" ]; then if [ $metod = "1" ]; then
screen -S $srver -X eval 'stuff "say сохранение данных..."\015'; else screen -S $srver -X eval 'stuff "broadcast сохранение данных..."\015'; fi; fi
screen -S $srver -X eval 'stuff "save-all"\015'
screen -S $srver -X eval 'stuff "save-all flush"\015'
asw=$(grep asw= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
intsav=$(grep intsav= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
metod=$(grep metod= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
brsav=$(grep brsav= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-); done